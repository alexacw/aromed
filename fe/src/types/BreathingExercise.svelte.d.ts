import { SvelteComponent, SvelteComponentTyped } from "svelte";
import PageComponent from "./PageComponent"
export default class BreathingExercise extends SvelteComponentTyped<{}, {}, {}>
    , PageComponent {
    canExit: () => boolean
}