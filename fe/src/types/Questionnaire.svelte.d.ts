import { SvelteComponent, SvelteComponentTyped } from "svelte";
import PageComponent from "./PageComponent"

export default class Questionnaire extends SvelteComponentTyped<{}, {}, {}>
    , PageComponent {
    canExit: () => boolean
}