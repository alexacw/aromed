export default interface PageComponent {
    canExit: () => boolean
}