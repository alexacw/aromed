
import { Writable, writable } from 'svelte/store';
export const beResWritable: Writable<Map<string, number[]>> = writable(new Map<string, number[]>());