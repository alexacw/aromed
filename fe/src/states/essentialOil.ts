
import { Writable, writable } from 'svelte/store';

export interface EssentialOil {
    name: string,
    type: string,
    brand: string,
    imagePath: string,
    url: string | null,
}

export const TProp = [
    "manage pain.",
    "improve sleep quality.",
    "reduce stress, agitation, and anxiety.",
    "soothe sore joints.",
    "treat headaches and migraines.",
    "alleviate side effects of chemotherapy.",
    "ease discomforts of labor.",
    "fight bacteria, virus, or fungus.    ",
];

export const EssentialOils: EssentialOil[] = [
    {
        brand: null,
        imagePath: null,
        name: "Control",
        type: "Control",
        url: null
    },
    {
        brand: "Unclin",
        imagePath: "./img/unclin_lavender.png",
        name: "Unclin Lavender",
        type: "Lavender",
        url: "https://www.hktvmall.com/hktv/en/main/AAontop/s/H8508001/Skincare-%26-Makeup/Skincare-%26-Makeup/Chest-%26-Neck-Care-Massage-Oil/Massage-Oil/Unclin-100-pure-natural-ingredients-Lavender-essential-oil-no-additives-foot-bath-spa-bath/p/H8508001_S_c0011"
    }, {
        brand: "Unclin",
        imagePath: "./img/unclin_sandalwood.png",
        name: "Unclin Sandalwood",
        type: "Sandalwood",
        url: "https://www.hktvmall.com/hktv/en/main/AAontop/s/H8508001/Skincare-%26-Makeup/Skincare-%26-Makeup/Chest-%26-Neck-Care-Massage-Oil/Massage-Oil/Unclin-100-pure-natural-ingredientsSandalwood-essential-oilno-additives-foot-bath-spa-bath/p/H8508001_S_c0022"
    }, {
        brand: "ABOUTHAI",
        imagePath: "./img/abouthai_eucalyptus.png",
        name: "ABOUTHAI Eucalyptus",
        type: "Eucalyptus",
        url: "https://www.hktvmall.com/hktv/zh/main/%E9%98%BF%E5%B8%83%E6%B3%B0%E8%B6%85%E5%A0%B4/s/H7027001/%E8%AD%B7%E7%90%86%E4%BF%9D%E5%81%A5/%E8%AD%B7%E7%90%86%E4%BF%9D%E5%81%A5/%E8%AD%B7%E8%86%9A%E7%94%A8%E5%93%81/%E7%B2%BE%E6%B2%B9/100%E7%B4%94%E9%A6%99%E8%96%B0%E7%B2%BE%E6%B2%B9-%E6%B3%B0%E5%9C%8B%E9%9D%92%E6%AA%B8-30ml/p/H7027001_S_ABOUTHAI-8857103345024"
    }
]
export const EOTypes = ["Lavender", "Sandalwood", "Eucalyptus", "Kaffir Lime"];

export const EOBrands = [
    "Plant Therapy",
    "Vitruvi",
    "Aura Cacia",
    "Public Goods",
    "Eden’s Garden ",
    "Now Foods",
    "Rocky Mountain Oils",
    "Unclin",
    "ABOUTHAI",
];