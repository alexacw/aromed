import { now } from 'svelte/internal';
import { Writable, writable } from 'svelte/store';

export const count = writable(0);
export const oximeterRawReadings: Writable<Array<SensorReading<RawReading>>> = writable([])
export const oximeterReadingNumbers: Writable<OximeterReading | null> = writable(null)
export const eoPreferences: Writable<EOPreferences> = writable(
    {
        eo: [],
        priceRange: { max: 1000, min: 0 },
        tProp: [],
        eoBrands: [],
    });
export interface OximeterReading {
    spo2: number;
    heart_rate: number;
    pi: number;
}
export type RawReading = number;


interface EOPreferences {
    eo: string[];
    tProp: string[];
    eoBrands: string[];
    priceRange: { min: number; max: number };
}
;

class SensorReading<T>{
    value: T;
    timestamp: number;
}


const ws = new WebSocket("ws://localhost:8080/ws/")
count.subscribe(value => {
    if (ws.readyState == EventSource.OPEN) {
        ws.send(JSON.stringify(value));
    }

})

function isOximeterReading(reading: OximeterReading | number): reading is OximeterReading {
    return (<OximeterReading>reading).spo2 !== undefined;
}

ws.onmessage = function (event) {
    let reading: OximeterReading | number = JSON.parse(event.data)
    if (isOximeterReading(reading)) {
        oximeterReadingNumbers.set(reading)
    } else {
        let x = reading;
        oximeterRawReadings.update((oldData) => [{ timestamp: Date.now(), value: x }])
    }
}