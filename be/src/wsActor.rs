use crate::{lib::oximeter::OximeterReading, readerActor::OximeterStreamer};
use actix::prelude::*;
use actix::{Actor, AsyncContext, StreamHandler};
use actix_web_actors::ws;

use log::{debug, info};

pub struct OximeterWsStreamer {
    pub tty_path: &'static str,
}

impl Actor for OximeterWsStreamer {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        let streamer = OximeterStreamer::new(ctx.address(), self.tty_path);
        Some(streamer.start());
    }

    fn stopped(&mut self, _ctx: &mut Self::Context) {}
}

impl Handler<OximeterReading> for OximeterWsStreamer {
    type Result = ();

    fn handle(&mut self, msg: OximeterReading, ctx: &mut Self::Context) -> Self::Result {
        debug!("OximeterWsStreamer got {:?}", msg);
        ctx.text(serde_json::to_string(&msg).unwrap());
    }
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for OximeterWsStreamer {
    fn handle(&mut self, item: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        match item {
            Ok(ws::Message::Ping(msg)) => ctx.pong(&msg),
            Ok(ws::Message::Text(text)) => ctx.text(text),
            Ok(ws::Message::Binary(bin)) => ctx.binary(bin),
            _ => (ctx.stop()),
        }
    }
}
