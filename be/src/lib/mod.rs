use std::{io::Error, time::Instant};

use async_trait::async_trait;

pub mod oximeter;

#[async_trait]
pub(crate) trait SensorReader<ReadingType> {
    async fn get_readings(&self) -> Result<Vec<Timestamped<ReadingType>>, Error>;
}

pub struct Timestamped<ReadingType> {
    r: ReadingType,
    t: Instant,
}
