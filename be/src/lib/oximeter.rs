use serde::{Deserialize, Serialize};
use std::{
    fmt::Write,
    io::{self, Result},
};

use actix::Message;
use bytes::{Buf, BytesMut};
use regex::Regex;
use tokio_serial::SerialPortBuilderExt;
use tokio_serial::SerialStream;
use tokio_util::codec::{Decoder, Encoder, Framed};

pub fn make_stream(tty_path: &str, baud_rate: u32) -> Result<Framed<SerialStream, OximeterCodec>> {
    let mut port = tokio_serial::new(tty_path, baud_rate).open_native_async()?;

    #[cfg(unix)]
    port.set_exclusive(false)
        .expect("Unable to set serial port exclusive to false");

    let reader = OximeterCodec.framed(port);

    Ok(reader)
}

#[derive(Message, Debug, Serialize)]
#[rtype(result = "()")]
#[serde(untagged)]
pub enum OximeterReading {
    Calculated(CalculatedOximeterReading),
    Raw(u32),
}

#[derive(Debug, Serialize)]
pub enum OximeterCommand {
    CalculatedOnly,
    RawAndCalculated,
}

#[derive(Debug, Serialize)]
pub struct CalculatedOximeterReading {
    pub spo2: f32,
    pub heart_rate: f32,
    pub pi: f32,
}

pub struct OximeterCodec;

impl Decoder for OximeterCodec {
    type Item = OximeterReading;
    type Error = io::Error;

    fn decode(
        &mut self,
        src: &mut BytesMut,
    ) -> core::result::Result<Option<Self::Item>, Self::Error> {
        if let Some(s) = src
            .as_ref()
            .iter()
            .position(|b| !(*b == b'\n' || *b == b'\r'))
        {
            src.advance(s);
        }

        let newline = src.as_ref().iter().position(|b| *b == b'\n' || *b == b'\r');
        let n = if let Some(i) = newline {
            i
        } else {
            return Ok(None);
        };
        let line = src.split_to(n);
        let s = std::str::from_utf8(line.as_ref())
            .map_err(|_| io::Error::new(io::ErrorKind::Other, "Invalid String"))?;

        if s.starts_with("U1:") {
            s["U1:".len()..]
                .parse::<u32>()
                .map_err(|_e| io::Error::new(io::ErrorKind::Other, "invalid U1 value"))
                .map(|raw_num| Some(OximeterReading::Raw(raw_num)))
        } else if s.starts_with("U2:") {
            let parsed_strs = Regex::new(r"(\d+),(\d+),(\d+)")
                .unwrap()
                .captures(&s["U2:".len()..])
                .ok_or(io::Error::new(io::ErrorKind::Other, "invalid U2 value"))?;
            if parsed_strs.len() != 4 {
                return Err(io::Error::new(io::ErrorKind::Other, "invalid U2 value"));
            }
            let parsed_numbers = parsed_strs
                .iter()
                .skip(1)
                .filter_map(|m| {
                    if let Some(m) = m {
                        m.as_str().parse::<u32>().ok()
                    } else {
                        None
                    }
                })
                .collect::<Vec<_>>();
            if parsed_numbers.len() != 3 {
                Err(io::Error::new(io::ErrorKind::Other, "invalid U2 value"))
            } else {
                Ok(Some(OximeterReading::Calculated(
                    CalculatedOximeterReading {
                        heart_rate: parsed_numbers[1] as f32,
                        spo2: parsed_numbers[0] as f32,
                        pi: parsed_numbers[2] as f32,
                    },
                )))
            }
        } else {
            Ok(None)
        }
    }
}

impl Encoder<OximeterCommand> for OximeterCodec {
    type Error = io::Error;
    fn encode(
        &mut self,
        _item: OximeterCommand,
        _dst: &mut BytesMut,
    ) -> core::result::Result<(), Self::Error> {
        match _item {
            OximeterCommand::CalculatedOnly => _dst.write_str("AT+MD:1\r\n"),
            OximeterCommand::RawAndCalculated => _dst.write_str("AT+MD:0\r\n"),
        }
        .map_err(|e| io::Error::new(io::ErrorKind::Other, format!("write failed: {}", e)))?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::{make_stream, OximeterCommand};
    use futures::SinkExt;
    use futures::StreamExt;

    #[tokio::test]
    async fn get_things_from_serial() {
        let mut reader = make_stream("/dev/ttyUSB0", 115200).expect("cannot open serial port");
        let mut cnt = 0;
        reader
            .send(OximeterCommand::RawAndCalculated)
            .await
            .unwrap();
        while let Some(reading) = reader.next().await {
            cnt += 1;
            if cnt > 1000 {
                return;
            }
        }
    }
}
