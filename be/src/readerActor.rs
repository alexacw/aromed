use actix::{
    dev::ToEnvelope, fut::wrap_future, Actor, Addr, AsyncContext, Context, Handler, SpawnHandle,
};
use futures::{SinkExt, StreamExt};
use log::{info, debug};
use tokio_serial::SerialStream;
use tokio_util::codec::Framed;

use crate::lib::oximeter::{make_stream, OximeterCodec, OximeterCommand, OximeterReading};

pub struct OximeterStreamer<A: Actor>
where
    A: Handler<OximeterReading>,
    A::Context: ToEnvelope<A, OximeterReading>,
{
    frame: Option<Framed<SerialStream, OximeterCodec>>,
    target_addr: Option<Addr<A>>,
    spawn_handle: Option<SpawnHandle>,
}

impl<A> OximeterStreamer<A>
where
    A: Actor,
    A: Handler<OximeterReading>,
    A::Context: ToEnvelope<A, OximeterReading>,
{
    pub fn new(a: Addr<A>, tty_path:&str) -> Self {
        let frame = make_stream(tty_path, 115200).expect("cannot open serial port");

        let addr = a;
        return Self {
            frame: Some(frame),
            spawn_handle: None,
            target_addr: Some(addr),
        };
    }
}

impl<A: Actor> Actor for OximeterStreamer<A>
where
    A: Handler<OximeterReading>,
    A::Context: ToEnvelope<A, OximeterReading>,
{
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        let mut frame = self.frame.take().unwrap();
        let addr = self.target_addr.take().unwrap();
        let future = async move {
            if let Err(_) = frame.send(OximeterCommand::RawAndCalculated).await {
                return;
            };
            while let Some(reading) = frame.next().await {
                if let Ok(reading) = reading {
                    debug!("{}", serde_json::to_string(&reading).unwrap());
                    let res = addr.send(reading).await;
                    if let Err(_x) = res {
                        return;
                    }
                }
            }
        };
        let wf = wrap_future::<_, Self>(future);
        self.spawn_handle = Some(ctx.spawn(wf));
    }

    fn stopped(&mut self, _ctx: &mut Self::Context) {}
}
