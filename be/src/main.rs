pub mod lib;
pub mod readerActor;
pub mod wsActor;
use actix_files as fs;
use actix_files::NamedFile;
use actix_web::{
    middleware, web, App, Error, HttpRequest, HttpResponse, HttpResponseBuilder, HttpServer,
    Responder, Result,
};
use actix_web_actors::ws;
use lazy_static::lazy_static;
use lib::oximeter::{make_stream, OximeterCodec};
use log::info;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use tokio_serial::SerialStream;
use tokio_util::codec::Framed;

use crate::wsActor::OximeterWsStreamer;

lazy_static! {
    static ref cfg: MyConfig = confy::load_path("./config.toml").unwrap();
}

fn build_framed_streamer(tty_path: &str, baud_rate: u32) -> Framed<SerialStream, OximeterCodec> {
    make_stream(&tty_path, baud_rate).expect(&format!("cannot open serial port at {}", tty_path))
}

async fn endpoint_ws(req: HttpRequest, stream: web::Payload) -> Result<HttpResponse, Error> {
    info!("endpoint_ws");
    let actor = OximeterWsStreamer {
        tty_path: &cfg.tty_path,
    };
    let resp = ws::start(actor, &req, stream);
    println!("{:?}", resp);
    resp
}

async fn index(_req: HttpRequest) -> Result<HttpResponse> {
    return Ok(HttpResponse::MovedPermanently()
        .append_header(("Location", "/index.html"))
        .body(()));
}
#[derive(Serialize, Deserialize)]
struct MyConfig {
    pub tty_path: String,
    pub baud_rate: u32,
}
/// `MyConfig` implements `Default`
impl ::std::default::Default for MyConfig {
    fn default() -> Self {
        Self {
            tty_path: "/dev/ttyUSB0".into(),
            baud_rate: 115200,
        }
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    {
        build_framed_streamer(&cfg.tty_path, cfg.baud_rate);
    }
    env_logger::init();
    HttpServer::new(|| {
        App::new()
            .wrap(middleware::NormalizePath::trim())
            .route("/", web::get().to(index))
            .route("/ws", web::get().to(endpoint_ws))
        // .service(fs::Files::new("/", "../fe/public").show_files_listing())
    })
    .bind("localhost:8080")?
    .run()
    .await
}
