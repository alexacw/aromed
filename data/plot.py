#%%
import re

with open("./cutecom.log", errors="ignore") as f:
    x = [
        int(re.match("(?:U1:)(\d+)", l).groups()[0])
        for l in f.readlines()
        if l.startswith("U1")
    ]
x

# %%
import plotly.express as px

px.line(x).show()
# %%

# %%
